import random

bordgrootte = 10
schepen_geraakt = 0

# Maximaal 9 schepen
aantal_schepen = 9

schipbord = []
spelersbord = []

for i in range(bordgrootte):
    # Maak 2 matrixen aan, 1 voor de schoten van de speler en de andere voor de plaats van de schepen
    spelersbord.append(["."] * bordgrootte)
    schipbord.append(["."] * bordgrootte)

# Dit zijn de icoontjes van de verschillende schepen
icons = ["!", "@", "#", "$", "%", "^", "&", "*", "~"]

for i in range(0, aantal_schepen):

    schipgrootte = random.randint(2, 3)

    # Pak laatste item uit icons
    ship_icoon = icons.pop()

    # Kies een random rij en kolom tussen 0 en bordgrootte - 1
    schip_rij = random.randint(0, bordgrootte - 1)
    schip_kolom = random.randint(0, bordgrootte - 1)

    if schipbord[schip_rij][schip_kolom] == ".":
        schipbord[schip_rij][schip_kolom] = ship_icoon
    # else:
    #     pass

    if schipgrootte == 2:
        # Vul 3 posities van schip
        for j in range(3):
            if schip_rij + j > (bordgrootte - 1):
                for k in range(0, 2):
                    if schipbord[schip_rij - k][schip_kolom] == ship_icoon:
                        pass
                    else:
                        schipbord[schip_rij - k][schip_kolom] = ship_icoon
            else:
                schipbord[schip_rij + j][schip_kolom] = ship_icoon
    else:
        # Vul 2 posities van schip
        for j in range(2):
            if schip_rij + j > (bordgrootte - 1):
                for k in range(2):
                    if schipbord[schip_rij - k][schip_kolom] != ship_icoon:
                    #     pass
                    # else:
                        schipbord[schip_rij - k][schip_kolom] = ship_icoon
            else:
                schipbord[schip_rij + j][schip_kolom] = ship_icoon


#Laat de schepen zien
def printShips():
    print('   0 1 2 3 4 5 6 7 8 9')
    for positie, rij in enumerate(schipbord):
        print(str(positie).rjust(2,' ') + "|" + "|".join(rij))

#Laat het bord voor de speler zien
def printBord():
    print('   0 1 2 3 4 5 6 7 8 9')
    for positie, rij in enumerate(spelersbord):
        print(str(positie).rjust(2,' ') + "|" + "|".join(rij))


for beurt in range(10):
    # printShips()
    printBord()
    print("Beurt: " + str(beurt + 1))
    try:
        user_rij = int(input("Welke rij? "))
        user_kolom = int(input("Welke kolom? "))

        if (user_rij >= bordgrootte or user_kolom >= bordgrootte) or (user_kolom < 0 or user_rij < 0):
            print("Die staat niet op het bord")

        elif schipbord[user_rij][user_kolom] == "X":
            print("Die heb je al geraden!")

        elif schipbord[user_rij][user_kolom] != ".":
            print("Je hebt een schip geraakt!")

            for i in range(1, 3):
                # Controleer of opgegeven rij + 1 of 2 extra plaatsen kleiner dan bord zijkant is
                if user_rij + i < bordgrootte:
                    if schipbord[user_rij + i][user_kolom] == schipbord[user_rij][user_kolom]:
                        schipbord[user_rij + i][user_kolom] = "X"
                        spelersbord[user_rij + i][user_kolom] = "X"

                # Controleer of opgegeven rij - 1 of 2 groter dan bord zijkant is
                if user_rij - i >= 0:
                    if schipbord[user_rij - i][user_kolom] == schipbord[user_rij][user_kolom]:
                        schipbord[user_rij - i][user_kolom] = "X"
                        spelersbord[user_rij - i][user_kolom] = "X"

                # Controleer of opgegeven kolom + 1 of 2 groter dan bord zijkant is
                if user_kolom + i < bordgrootte:
                    if schipbord[user_rij][user_kolom + i] == schipbord[user_rij][user_kolom]:
                        schipbord[user_rij][user_kolom + i] = "X"
                        spelersbord[user_rij][user_kolom + i] = "X"

                # Controleer of opgegeven kolom - 1 of 2 groter dan bord zijkant is
                if user_kolom - i >= 0:
                    if schipbord[user_rij][user_kolom - i] == schipbord[user_rij][user_kolom]:
                        schipbord[user_rij][user_kolom - i] = "X"
                        spelersbord[user_rij][user_kolom - i] = "X"

            schipbord[user_rij][user_kolom] = "X"
            spelersbord[user_rij][user_kolom] = "X"

            if schepen_geraakt >= aantal_schepen:
                print("Gewonnen! Je hebt " + str(beurt) + " beurten gebruikt.")
                break
        else:
            print("Mis!")
            spelersbord[user_rij][user_kolom] = "X"
            schipbord[user_rij][user_kolom] = "X"

    except ValueError:
        print("Dat is geen geldig vakje!")

print("Game Over!")
print("Hier lagen de schepen:")
printShips()
